package assign.resources;

import javax.xml.parsers.*;

import org.xml.sax.InputSource;
import org.w3c.dom.*;

import assign.domain.Meeting;

import java.io.*;
import java.util.ArrayList;

public class ParseXmlString {

      public static ArrayList<Meeting> obtainedResultMeetings(String xmlRecords) {
          ArrayList<Meeting> result = new ArrayList<Meeting>();
          try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(xmlRecords));

                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("meeting");

                for (int i = 0; i < nodes.getLength(); i++) {
                    Meeting m = new Meeting();

                    Element e = (Element) nodes.item(i);
                    String tmn = e.getElementsByTagName("team_meeting_name").item(0).getTextContent();
                    String year = e.getElementsByTagName("year").item(0).getTextContent();
                    String mname = e.getElementsByTagName("meeting_name").item(0).getTextContent();
                    String link = e.getElementsByTagName("link").item(0).getTextContent();

                    m.setTeam_meeting_name(tmn);
                    m.setYear(year);
                    m.setMeeting_name(mname);
                    m.setLink(link);

                    result.add(m);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
          return result;
      }

      public static int obtainedMeetingCount(String xmlRecords) {
          int count = -1;
          try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(xmlRecords));

                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("count");

                count = Integer.parseInt(nodes.item(0).getTextContent());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
          return count;
      }

}
