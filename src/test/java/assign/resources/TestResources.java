package assign.resources;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Before;
import org.junit.Test;

import assign.domain.Meeting;
import static org.junit.Assert.*;

public class TestResources {

    @Before
    public void setUp() {

    }

    @Test
    public void testResource1() {
    	int expectedResult = 1;

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/count");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        System.out.println("xmlRecords: "+xmlRecords);
        int obtainedResult = ParseXmlString.obtainedMeetingCount(xmlRecords);

        assertEquals(expectedResult,obtainedResult);
       
    }
    
    @Test
    public void testResource2() {
        int expectedResult = 5;

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum_team_meeting/year/2013/count");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        int obtainedResult = ParseXmlString.obtainedMeetingCount(xmlRecords);

        assertEquals(expectedResult,obtainedResult);
       
    }
    
    @Test
    public void testResource3() {
        ArrayList<Meeting> expectedResult = new ArrayList<Meeting>();
        Meeting m = new Meeting();
        m.setTeam_meeting_name("solum");
        m.setYear("2014");
        m.setMeeting_name("solum.2014-07-29-16.03.log.html");
        m.setLink("http://eavesdrop.openstack.org/meetings/solum/2014/solum.2014-07-29-16.03.log.html");
        expectedResult.add(m);
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        ArrayList<Meeting> obtainedResult = ParseXmlString.obtainedResultMeetings(xmlRecords);

        assertEquals(expectedResult,obtainedResult);
       
    }
    
    @Test
    public void testResource4() {
        int expectedResult = 0;

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2013/count");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        int obtainedResult = ParseXmlString.obtainedMeetingCount(xmlRecords);

        assertEquals(expectedResult,obtainedResult);
       
    }
    
    
}