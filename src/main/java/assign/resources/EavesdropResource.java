package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import assign.domain.Meeting;
import assign.domain.Meetings;
import assign.etl.DBLoader;


@Path("/myeavesdrop")
public class EavesdropResource {
	
	
	
	public EavesdropResource(@Context ServletContext servletContext) {		
		
	}

@GET
@Path("/helloworld")
@Produces("text/html")
public String helloWorld() {
return "Hello world";
}


@GET
@Path("/meetings/{mname}/year/{year}")
@Produces("application/xml")
public Response getMeetings(@PathParam("mname") String mname, @PathParam("year") String year) throws Exception{
	if(!mname.equals("solum") && !mname.equals("solum_team_meeting")){
		System.out.println("Status Code: "+Response.Status.BAD_REQUEST);
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	if(!year.equals("2013") && !year.equals("2014") && !year.equals("2015")){
		System.out.println("Status Code: "+Response.Status.BAD_REQUEST);
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	DBLoader d = new DBLoader();
	final Meetings meetings = d.getMeetings(mname,year);
	if(meetings != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputMeetings(outputStream, meetings);
			}
		};
		return Response.ok(so).build();
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/meetings/{mname}/year/{year}/count")
@Produces("application/xml")
public Response getMeetingCount(@PathParam("mname") String mname, @PathParam("year") String year) throws Exception{
	if(!mname.equals("solum") && !mname.equals("solum_team_meeting")){
		System.out.println("Status Code: "+Response.Status.BAD_REQUEST);
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	if(!year.equals("2013") && !year.equals("2014") && !year.equals("2015")){
		System.out.println("Status Code: "+Response.Status.BAD_REQUEST);
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	DBLoader d = new DBLoader();
	final Meetings meetings = d.getMeetings(mname,year);
	int count = meetings.getMeetings().size();
	final String xmlString = "<meetings><count>"+count+"</count></meetings>";
	if(meetings != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		return Response.ok(so).build();
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}


protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(meetings, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
}


}