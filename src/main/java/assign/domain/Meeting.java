package assign.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "meetings" )
@XmlRootElement(name="meeting")
@XmlType(propOrder={"team_meeting_name","year","meeting_name","link"})
public class Meeting {
   
    private Long mid;
    private String team_meeting_name;
    private String year;
    private String meeting_name;
    private String link;
   
    public Meeting() {
        // this form used by Hibernate
    }
   
    public Meeting(String teamMeetingName, String year, String meetingName, String link) {
        // for application use, to create new meetings
        this.team_meeting_name = teamMeetingName;
        this.year = year;
        this.meeting_name = meetingName;
        this.link = link;
    }
   
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getMid() {
        return mid;
    }

    @SuppressWarnings("unused")
    private void setMid(Long mid) {
        this.mid = mid;
    }

    @Column(name="team_meeting_name")
    @XmlElement(name="team_meeting_name")
    public String getTeam_meeting_name() {
        return team_meeting_name;
    }

    public void setTeam_meeting_name(String teamMeetingName) {
        this.team_meeting_name = teamMeetingName;
    }
   
    @Column(name="year")
    @XmlElement(name="year")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
   
    @Column(name="meeting_name")
    @XmlElement(name="meeting_name")
    public String getMeeting_name() {
        return meeting_name;
    }

    public void setMeeting_name(String meetingName) {
        this.meeting_name = meetingName;
    }
   
    @Column(name="link")
    @XmlElement(name="link")
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;

    }
   
    public boolean equals(Object m){
    	if(m == null)
    		return false;
    	if(!(m instanceof Meeting))
    		return false;
    	Meeting m1 = (Meeting)m;
    	return this.team_meeting_name.equals(m1.team_meeting_name) && this.year.equals(m1.year)
    			&& this.meeting_name.equals(m1.meeting_name) && this.link.equals(m1.link);
    }
}

