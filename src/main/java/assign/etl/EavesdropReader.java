package assign.etl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EavesdropReader {
	
	String url;
	Logger logger; 

	
	public EavesdropReader(String url) {
		this.url = url;
		
		logger = Logger.getLogger("EavesdropReader");
	}
	
	/*
	 * Return a map where the contents of map is a single entry:
	 * <this.url, List-of-parsed-entries>
	 */
	public Map<String, List<String>> readData() {
		
		logger.info("Inside readData.");
		
		Map<String, List<String>> data = new HashMap<String, List<String>>();
		
		Document doc;
		
		try {
			
			// need http protocol
			doc = Jsoup.connect(url).get();
	 
	 
			// get all links
			Elements outerLinks = doc.select("a[href]");
			for(int i = 5; i < outerLinks.size(); i++){
				Element ele = outerLinks.get(i); 
				String year = ele.attr("href");
				doc = Jsoup.connect(url+year).get();
				Elements innerLinks = doc.select("a[href]");
				for(int j = 5; j < innerLinks.size(); j++){
					String link = innerLinks.get(j).attr("href");
					int index1 = link.indexOf(".");
					int index2 = link.indexOf(".",index1+1);
					int index3 = link.indexOf(".",index2+1);
					String prefix = link.substring(0,index3);
					List<String> a = data.get(prefix);
					if(a == null){
						ArrayList<String> l = new ArrayList<String>();
						l.add(link);
						data.put(prefix, l);
					}
					else{
						a.add(link);
						data.put(prefix,a);
					}
					
				}
			}
	 
		} catch (IOException e) {
			//e.printStackTrace();
			return data;
		}
				
		return data;
	}
}