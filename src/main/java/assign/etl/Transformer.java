package assign.etl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

public class Transformer {
	
	Logger logger;
	
	public Transformer() {
		logger = Logger.getLogger("Transformer");		
	}
	
	public Map<String, List<String>> transform(String url, Map<String, List<String>> data) {
		// Read the data;
		// transform it as required;
		// return the transformed data;
		
		logger.info("Inside transform.");

		Map<String, List<String>> newData = new HashMap<String, List<String>>();

		// transform data into newData
		List<String> longestLinks = new ArrayList<String>();
		for(String s : data.keySet()){
			List<String> l = data.get(s);
			String longest = selectLongest(l);
			longestLinks.add(longest);
		}
		Collections.sort(longestLinks);
		newData.put(url,longestLinks); 
		
		return newData;
	}

	private String selectLongest(List<String> l) {
		String longest = "";
		for(int i = 0; i < l.size(); i++){
			String s = l.get(i);
			if(s.length() > longest.length())
				longest = s;
		}
		return longest;
	}
}

