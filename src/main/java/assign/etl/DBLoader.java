package assign.etl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import assign.domain.Meeting;
import assign.domain.Meetings;

import java.util.logging.*;

public class DBLoader {
	private SessionFactory sessionFactory;
	
	Logger logger;
	
	public DBLoader() {
		// A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
        
        logger = Logger.getLogger("EavesdropReader");
	}
	
	public void loadData(Map<String, List<String>> data) throws Exception {
		logger.info("Inside loadData.");
		for(String url : data.keySet()){
			List<String> value = data.get(url);
			for(int i = 0; i < value.size(); i++){
				String meeting_name = value.get(i);
				int index = meeting_name.indexOf(".");
				String team_meeting_name = meeting_name.substring(0,index);
				String year = meeting_name.substring(index + 1, index + 5);
				String link = url + year + "/" + meeting_name;
				addMeeting(team_meeting_name,year,meeting_name,link);
			}
		}
	}
	
	public Long addMeeting(String tmn, String year, String mn,String link) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long mid = null;
		try {
			tx = session.beginTransaction();
			Meeting meeting = new Meeting(tmn,year,mn,link); 
			session.save(meeting);
			mid = meeting.getMid();
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return mid;
	}
	
	
	public Meetings getMeetings(String name, String year) throws Exception {
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Meeting.class).
        		add(Restrictions.eq("team_meeting_name", name)).
        		add(Restrictions.eq("year", year));
		
		Meetings meetings = new Meetings();
		meetings.setMeetings(criteria.list());
		
		tx.commit();
		
		return meetings;		
	}
	
}